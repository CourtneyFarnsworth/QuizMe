var aws = require('aws-sdk');
var dynamodb = new aws.DynamoDB();

exports.handler = (event, context, callback) => {
    dynamodb.scan({TableName: 'quiz_me_quizzes'}, (err, data) => {
        callback(null, data['Items']);
    });
};