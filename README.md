# 389L AWS Spring 2018 Project


## Quiz Me
QuizMe is the easiest way to study and improve your knowledge on Machine Learning. With just a few clicks, you can review the questions answer set for machine learning and then use Amazon Alexa to ask you questions to make studying fun.

<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=3 orderedList=false} -->

<!-- code_chunk_output -->

* [389L AWS Spring 2018 Project](#389l-aws-spring-2018-project)
	* [Quiz Me](#quiz-me)
		* [AWS Services](#aws-services)
		* [Architecture Diagram](#architecture-diagram)
		* [User Information](#user-information)
		* [Host Information](#host-information)

<!-- /code_chunk_output -->
### AWS Services
* AWS Lambda Function
* Alexa Skill Kit
* DynamoDB: Stores quiz state and user data
* S3: Hosts HTML and CSS for a website

### Architecture Diagram
![alt text](/readme_imgs/diagram.png)

### User Information
1. Check out the website to see how to use the quiz me app with Alexa.

__Learn about the application on the home page and how to use to application with Alexa or look for a quiz to take__
![alt text](/readme_imgs/site_quiz1.png)
__Review the question/answer set or print it for more studying__
![alt text](/readme_imgs/site_quiz2.png)

2. Use your Alexa enabled devices to take the quiz
__Usage Example__
<b>Alexa, open quiz me</b>
<i>Welcome to Quiz Me! You can ask me to start a quiz when you're ready.</i>
<b>Start a quiz</b>
<i>OK. I will ask you 10 questions. Here is your 1th question. <code>Question</code>.</i>
<b>Your Answer</b>
<ul>
<li>If you are correct: <br>
	<i><code>Positive Words</code>! The Answer of  <code>Question</code> is  <code>Answer</code>. Your current score is 1 out of 1. Here is your 2th question.  <code>Question</code></i></li>
<li>If you are incorrect: <br>
	<i><code>Negative Words</code>! The Answer of  <code>Question</code> is  <code>Answer</code>. Your current score is 0 out of 1. Here is your 2th question.  <code>Question</code></i></li>
<li>.......</li>
</ul>
<br>
<i>Your final score is 1 out of 10. Thank you for studying with Quiz Me!</i>


### Host Information
1. Download the website file in this repo and upload these into your AWS S3 Buckets.
2. Download the `ask_interaction.json` file, set up a new skill and upload this to your [Amazon Developer Portal](https://developer.amazon.com) in the tab JSON editor you can just copy paste this code.
3. Download the `lambda_quizme.js` file and create a new lambda function in your [AWS Console](https://aws.amazon.com/console/).
4. Download the `get_quizzes.js` file and create a new lambda function in your [AWS Console](https://aws.amazon.com/console/).
5. Set up a database in DynamoDB in your [AWS Console](https://aws.amazon.com/console/). called `quiz_me_quizzes` with the items that you add in the following format:
```javascript 
{
  "Answers": [
    "Decision Boundary"
  ],
  "Questions": [
    "What is the line that seperates positives and negatives"
  ],
  "Title": "Machine Learning"
}
```
6. Set up your API gateway in your [AWS Console](https://aws.amazon.com/console/) we will add one request: a get request using the lambda function `quiz_me_quizzes`.
7. Enjoy!
