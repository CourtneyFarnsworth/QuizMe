{
  "Answers": [
    "Decision Boundary",
    "Overfitting",
    "Underfitting",
    "True",
    "Not linearly separable",
    "10",
    "45",
    "True",
    "False",
    "True"
  ],
  "Questions": [
    "What is the line that seperates positives and negatives?",
    "If you train a learning algorithm and get 10% training error and 40% test error, the model is probably?",
    "If you train a learning algorithm and get 40% training error and 10% test error, the model is probably?",
    "True of False, Random initialization helps address overfitting during perceptron training?",
    "If the margin for a data set is strictly less than zero, then the data is?",
    "If you have 10 classes and are doing OVA classification, how many classifiers do you need to train?",
    "If you have 10 classes and are doing AVA classification, how many classifiers do you need to train?",
    "True or False, The gradient of f at x is always also a subgradient of f at x?",
    "True of False, The naive Bayes model assumes that all the features of an example are independent?",
    "True of False, A 2-layer neural network can approximate a function to any given error rate?"
  ],
  "Title": "Machine Learning"
}
