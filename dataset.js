// This file is for testing and needs to be debugged
// The function needs to be added to the lambda_quizme.js when getDataSet() works
// Test in terminal with node dataset.js

var data = [
                {Question: "What is my favorite color",        Answer: "blue" },
                {Question: "What is my favorite color",        Answer: "blue" },
                {Question: "What is my favorite color",        Answer: "blue" },
                {Question: "What is my favorite color",        Answer: "blue" },
                {Question: "What is my favorite color",        Answer: "blue" },
                {Question: "What is my favorite color",        Answer: "blue" }
            ];


// TODO: Debug function
// Function to add to the lambda_quizme.js
function getDataSet(){
  var data = new Array();
  // Load the SDK for JavaScript
  var AWS = require("aws-sdk");
  // Set the region 
  AWS.config.update({region: 'us-east-1'});
  // Access datbase
  var dynamodb = new AWS.DynamoDB();
  // Set up params for query
  var params = {
    Key: {
      "Title": {
        S: "Computer Architecture"
      },
    }, 
    TableName: "quiz_me_quizzes"
  };
 
  // Query database
  dynamodb.getItem(params, function(err, d) {
    if (err){ 
      // an error occurred
      console.log(err, err.stack);
    } else {
      // success
      // load questions and answers
      for (var i in d.Item.Questions.L) { 
        //debug I got info from DB
        //console.log(d.Item.Questions.L[i]['S'] + " " + d.Item.Answers.L[i]['S']);
      
        //remove question mark
        d.Item.Questions.L[i]['S'] = d.Item.Questions.L[i]['S'].slice(0, -1);
        // Set up new object
        let QA_item = {"Question": d.Item.Questions.L[i]['S'],"Answer": d.Item.Answers.L[i]['S']};
        data.push(new Object(QA_item));
        //console.log('{ Question: \'' + d.Item.Questions.L[i]['S'] + '\', Answer: \'' + d.Item.Answers.L[i]['S'] + '\'}'); 
      }
      console.log(data);
      return data;
    }   
  });
}

// TODO: debug funciton above this should be pass by reference 
// but acting like pass by value
// the dataset is changed within the function but no changes to dataset 
// after return from function

data = getDataSet();
console.log(data);

for (var i in data) {
  console.log(data[i]);
}


